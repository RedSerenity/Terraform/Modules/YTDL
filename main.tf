data "docker_registry_image" "YouTubeDl" {
	name = "tzahi12345/youtubedl-material:latest"
}

resource "docker_image" "YouTubeDl" {
	name = data.docker_registry_image.YouTubeDl.name
	pull_triggers = [data.docker_registry_image.YouTubeDl.sha256_digest]
}

module "YouTubeDl" {
  source = "gitlab.com/RedSerenity/docker/local"

	name = var.name
	image = docker_image.YouTubeDl.latest

	networks = [{ name: var.docker_network, aliases: ["ytdl.${var.internal_domain_base}"] }]
  ports = [{ internal: 17442, external: 9608, protocol: "tcp" }]
	volumes = [
		{
			host_path = "${var.docker_data}/YouTubeDl"
			container_path = "/app/appdata"
			read_only = false
		},
		{
			host_path = "${var.audio}"
			container_path = "/app/audio"
			read_only = false
		},
		{
			host_path = "${var.video}"
			container_path = "/app/video"
			read_only = false
		},
		{
			host_path = "${var.subscriptions}"
			container_path = "/app/subscriptions"
			read_only = false
		},
		{
			host_path = "${var.users}"
			container_path = "/app/users"
			read_only = false
		}
	]

	environment = {
		"UID": "${var.uid}",
		"GID": "${var.gid}",
		"ALLOW_CONFIG_MUTATIONS": "true"
	}

	stack = var.stack
}
